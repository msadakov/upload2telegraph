package upl2tlgph

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/textproto"

	"github.com/pkg/errors"
)

const telegraphUploadURL = "https://telegra.ph/upload"

type respJSONS []struct {
	Src string `json:"src"`
}

// func main() {
// 	if len(os.Args) == 1 {
// 		log.Fatalln("Use: upload2telegraph file.png")
// 	}
//
// 	for _, filePath := range os.Args[1:] {
// 		fileBody, _ := ioutil.ReadFile(filePath)
// 		r := bytes.NewReader(fileBody)
//
// 		link, err := GetLink(r, "image/jpeg")
// 		if err != nil {
// 			log.Fatal(err)
// 		}
//
// 		fmt.Printf("%s => %s\n", filePath, link)
// 	}
// }

func GetLink(r io.Reader, fileContentType string) (string, error) {
	resp, err := uploadFile(r, fileContentType)
	if err != nil {
		return "", errors.Wrap(err, "can't make request")
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)

	var respJSON respJSONS

	err = decoder.Decode(&respJSON)
	if err != nil {
		return "", errors.Wrap(err, "can't parse response json")
	}

	return fmt.Sprintf("http://telegra.ph%s", respJSON[0].Src), nil
}

func createFormFile(w *multipart.Writer, contentType string) (io.Writer, error) {
	h := make(textproto.MIMEHeader)
	h.Set("Content-Disposition", fmt.Sprintf(`form-data; name="file"; filename="blob"`))
	h.Set("Content-Type", contentType)
	return w.CreatePart(h)
}

func uploadFile(r io.Reader, fileContentType string) (*http.Response, error) {
	body, contentType, err := makeData(r, fileContentType)
	if err != nil {
		return nil, errors.Wrap(err, "can't get body")
	}

	resp, err := http.Post(telegraphUploadURL, contentType, body)
	if err != nil {
		return nil, errors.Wrap(err, "can't make request")
	}

	return resp, err
}

func makeData(r io.Reader, contentType string) (io.Reader, string, error) {
	b := &bytes.Buffer{}
	writer := multipart.NewWriter(b)

	data, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, "", errors.Wrap(err, "can't open file")
	}

	part, err := createFormFile(writer, contentType)
	if err != nil {
		return nil, "", err
	}

	_, err = part.Write(data)
	if err != nil {
		return nil, "", err
	}

	err = writer.Close()
	if err != nil {
		return nil, "", errors.Wrapf(err, "writer close error")
	}

	return b, writer.FormDataContentType(), nil
}
